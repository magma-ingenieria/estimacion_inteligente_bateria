# Run
- `python3 -m venv estimacion-bateria-env`
- `python3 -m pip install --upgrade pip`
- `python3 -m pip install -r requirements.txt`
- `jupyter-notebook`
- `notebooks/notebook_551_Mixed1.ipynb`

<br>



# Refrences

- [LG 18650HG2 Li-ion Battery Data and Example Deep Neural Network xEV SOC Estimator Script](https://data.mendeley.com/datasets/cp3473x7xv/3)
- [Panasonic 18650PF Li-ion Battery Data](https://data.mendeley.com/datasets/wykht8y7tg/1)
- [LG 18650HG2 Li-ion Battery Data](https://data.mendeley.com/datasets/b5mj79w5w9)
- [Li-Ion Battery charge/discharge benchmark](https://data.mendeley.com/datasets/r4n22f4jfk/1)
